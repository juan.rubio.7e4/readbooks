package cat.itb.dam.m07.readbooks.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import cat.itb.dam.m07.readbooks.R;
import cat.itb.dam.m07.readbooks.model.Book;


public class FragmentAdd extends Fragment {

    Book book = new Book();

    Spinner spinnerStatus;
    EditText title;
    EditText author;
    EditText genre;
    RatingBar rating;
    Button add;

    NavController navController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add, container, false);

        spinnerStatus = view.findViewById(R.id.spinner);
        title = view.findViewById(R.id.editTitle);
        author = view.findViewById(R.id.editAuthor);
        genre = view.findViewById(R.id.editGenre);
        rating = view.findViewById(R.id.ratingBar);
        add = view.findViewById(R.id.button);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.spinner, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(adapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Book bookToUpdate = FragmentAddArgs.fromBundle(getArguments()).getBook();
        final int bookPosition = FragmentAddArgs.fromBundle(getArguments()).getItemPosition();

        if (bookToUpdate != null){
            book = FragmentAddArgs.fromBundle(getArguments()).getBook();
            title.setText(book.getName());
            author.setText(book.getAuthor());
            genre.setText(book.getGenre());
            rating.setRating(book.getRating());
            spinnerStatus.setSelection(book.getStatus());
        }

        navController = Navigation.findNavController(view);

        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 2){
                    rating.setClickable(true);
                    rating.setEnabled(true);
                }else {
                    rating.setClickable(false);
                    rating.setEnabled(false);
                    rating.setRating(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (title.getText().toString().isEmpty() || author.getText().toString().isEmpty() || genre.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "No field must be empty!", Toast.LENGTH_SHORT).show();
                }else {
                    book.setName(title.getText().toString());
                    book.setAuthor(author.getText().toString());
                    book.setGenre(genre.getText().toString());
                    book.setRating(rating.getRating());
                    book.setStatus(spinnerStatus.getSelectedItemPosition());


                    navController.navigate(FragmentAddDirections.actionFragmentAddToFragmentList().setBook(book).setItemPosition(bookPosition));
                }
            }
        });

    }
}