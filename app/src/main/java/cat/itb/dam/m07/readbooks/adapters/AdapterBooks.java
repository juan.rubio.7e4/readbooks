package cat.itb.dam.m07.readbooks.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.dam.m07.readbooks.R;
import cat.itb.dam.m07.readbooks.model.Book;

public class AdapterBooks extends RecyclerView.Adapter<AdapterBooks.BookViewHolder> {

    List<Book> books;
    OnItemClickListener itemClickListener;

    public interface OnItemClickListener {
        public void onItemClick(Book book, int position);
    }

    public AdapterBooks(List<Book> books, OnItemClickListener listener) {
        this.books = books;
        this.itemClickListener = listener;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        holder.bindData(this.books.get(position), this.itemClickListener);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    class BookViewHolder extends RecyclerView.ViewHolder{

        TextView author;
        TextView name;
        TextView genre;
        TextView status;
        RatingBar rating;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.titleTextView);
            author = itemView.findViewById(R.id.authorTextView);
            genre = itemView.findViewById(R.id.genreTextView);
            status = itemView.findViewById(R.id.statusTextView);
            rating = itemView.findViewById(R.id.ratingBar);
        }

        public void bindData(final Book book, final OnItemClickListener listener){

            if(book != null){
                name.setText(book.getName());
                author.setText(book.getAuthor());
                genre.setText(book.getGenre());
                rating.setRating(book.getRating());

                switch(book.getStatus()){
                    case 0:
                        status.setText("PENDING!");
                        status.setTextColor(Color.GRAY);
                        break;
                    case 1:
                        status.setText("READING!");
                        status.setTextColor(Color.GREEN);
                        break;
                    case 2:
                        status.setText("READ!");
                        status.setTextColor(Color.CYAN);
                        break;
                    default:
                        break;
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(book, getAdapterPosition());
                    }
                });

            }
        }
    }
}
