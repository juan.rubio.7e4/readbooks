package cat.itb.dam.m07.readbooks.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable {

    private String name;
    private String author;
    private String genre;
    private float rating;
    private int status;

    public Book() {
    }

    public Book(String name, String author, String genre, float rating, int status) {
        this.name = name;
        this.author = author;
        this.genre = genre;
        this.rating = rating;
        this.status = status;
    }

    protected Book(Parcel in) {
        name = in.readString();
        author = in.readString();
        genre = in.readString();
        rating = in.readFloat();
        status = in.readInt();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(author);
        dest.writeString(genre);
        dest.writeFloat(rating);
        dest.writeInt(status);
    }
}
