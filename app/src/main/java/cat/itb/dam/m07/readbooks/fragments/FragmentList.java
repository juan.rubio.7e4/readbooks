package cat.itb.dam.m07.readbooks.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import cat.itb.dam.m07.readbooks.R;
import cat.itb.dam.m07.readbooks.adapters.AdapterBooks;
import cat.itb.dam.m07.readbooks.model.Book;
import cat.itb.dam.m07.readbooks.model.BooksViewModel;

public class FragmentList extends Fragment {

    RecyclerView recyclerView;
    AdapterBooks adapter;
    BooksViewModel booksViewModel;
    NavController navController;

    Button add;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        booksViewModel = new ViewModelProvider(this).get(BooksViewModel.class);

        int itemPosition = FragmentListArgs.fromBundle(getArguments()).getItemPosition();
        Book book = FragmentListArgs.fromBundle(getArguments()).getBook();

        if (book != null){
            if (itemPosition != -1){
                booksViewModel.remove(itemPosition);
                booksViewModel.add(itemPosition , book);
            }else {
                booksViewModel.add(book);
            }
        }

        add =  view.findViewById(R.id.buttonAdd);

        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new AdapterBooks(booksViewModel.getBooks(), new AdapterBooks.OnItemClickListener() {
            @Override
            public void onItemClick(Book book, int position) {
                navController.navigate(FragmentListDirections.actionFragmentList2ToFragmentAdd().setBook(book).setItemPosition(position));
            }
        });
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);



        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(FragmentListDirections.actionFragmentList2ToFragmentAdd());
            }
        });
    }
}