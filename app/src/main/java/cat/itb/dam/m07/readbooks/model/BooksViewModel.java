package cat.itb.dam.m07.readbooks.model;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class BooksViewModel extends ViewModel {

    private static List<Book> books = new ArrayList<>();

    public BooksViewModel() {

    }

    public List<Book> getBooks(){
        return books;
    }

    public void add(Book book){
        this.books.add(book);
    }

    public void add(int position, Book book){
        this.books.add(position, book);
    }

    public void remove(int index){
        this.books.remove(index);
    }
}
